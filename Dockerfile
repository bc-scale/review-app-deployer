FROM docker:stable

ARG KUBECTL_VERSION=1.22.2
ARG HELM_VERSION=3.7.1

ENV HOME=/config

RUN apk add --no-cache curl ca-certificates git bash gettext

RUN wget -O /usr/local/bin/kubectl https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl && \
    chmod +x /usr/local/bin/kubectl && \
    kubectl version --client

RUN set -x && \
    wget -O /tmp/helm.tar.gz https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz && \
    tar -xzvf /tmp/helm.tar.gz && \
    mv linux-amd64/helm /usr/local/bin/helm && \
    rm -rf linux-amd64 /tmp/helm.tar.gz && \
    chmod +x /usr/local/bin/helm && \
    helm version

RUN curl -s "https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh" | bash && \
	mv kustomize /usr/local/bin/kustomize && \
	chmod +x /usr/local/bin/kustomize && \
	kustomize version

# Create non-root user (with a randomly chosen UID/GUI).
RUN adduser deployer -Du 2342 -h /config

USER deployer